# keywords_by_year

## Description

Display a bar graph with keywords. You can see how keywords evolve during year.

4 bar graphs are available :

* with keywords enter by authors (analyzer_year_pub_keywords)
* with analyze of abstract and title only unigram (analyzer_year_pub_unigram)
* with analyze of abstract and title at least bigram (analyzer_year_pub_bigram)
* with analyze of abstract and title only trigram (analyzer_year_pub)

To show the different graph you need to change this line of the code "with open("analyzer_year_pub_keywords", "rb") as fp:" l.35 in file bar_char_trigrams_year.py.

You need to change "analyzer_year_pub_keywords" to "analyzer_year_pub_unigram" if you want to see unigram instead of keywords enter by authors, or vice versa.

The green bar say that the words is a new one that appear for the current year display, a black one say the word was already in the last graph (last year).
At the end of the bar you can see the occurrence number of the word for the current year display.

## Libraries

You need pickle, matplotlib, numpy and operator

## Run the code 

$python3 bar_char_trigrams_year.py
