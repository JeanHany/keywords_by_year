import pickle
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import animation
import numpy as np
from operator import itemgetter

def animate(i):
    if i < len(list_tuple) :
        ax.clear()
        lt = list_tuple[i]
        y_pos = np.arange(len(lt[1]))
        if i == 0 :
            ax.barh(y_pos, lt[2], align='center',
                        color='black')
        else :
            color = []
            old_list = list_tuple[i - 1][1]
            new_list = lt[1]
            for n in new_list :
                if n in old_list :
                    color.append('black')
                else :
                    color.append('green' )
            ax.barh(y_pos, lt[2], align='center',
                        color=color)
        for i in range(0,10) :
            ax.text(lt[2][i] + 0.01, i, str(lt[2][i]), color='red')
        ax.set_yticks(y_pos)
        ax.set_yticklabels(lt[1])
        ax.set_title('Evolution of keywords by year '+str(lt[0]))
        ax.invert_yaxis()  # labels read top-to-bottom
        ax.set_xlabel('Nb keywords by year')

with open("analyzer_year_pub_keywords", "rb") as fp:   #Pickling
    list_tuple = pickle.load(fp)

list_tuple = sorted(list_tuple, key=itemgetter(0))
plt.rcdefaults()
plt.rcParams.update({'font.size': 14})
fig, ax = plt.subplots()

ani = animation.FuncAnimation(fig, animate, interval=5000)
plt.subplots_adjust(left=0.21, bottom=0.11, right=0.95, top=0.88, wspace=0.20, hspace=0.20)
plot_backend = plt.get_backend()
mng = plt.get_current_fig_manager()
if plot_backend == 'TkAgg':
	mng.resize(*mng.window.maxsize())
elif plot_backend == 'wxAgg':
	mng.frame.Maximize(True)
elif plot_backend == 'Qt4Agg':
    mng.window.showMaximized()

plt.show()
